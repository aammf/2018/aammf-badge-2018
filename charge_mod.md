# Modifying the board so it will charge while off [MEDIUM/HARD difficulty]

+ **Step 1:**

Cut the traces here, and expose copper to the right of both cuts:

![cut](gfx/charge_mod/01_cut_traces.jpg)

+ **Step 2:**

Wire up the DCDC converter like this:
Input to the DCDC goes to Batt+
Solder a wire between the two exposed traces from step 1. 

![wires](gfx/charge_mod/02_wire_up_dcdc.jpg)

+ **Step 3:**

Mount the BMS (Battery Management System) using pin headers.

![bms](gfx/charge_mod/03_mount_bms.jpg)

+ **Step 4:**

Mount the battery:

+ **\+** goes to **B+ on the BMS**,
+ **\-** goes to **Batt - on the main board**.

![batt](gfx/charge_mod/04_mount_battery.jpg)

**And you're done**

**Bonus images:**

+ Checking that the system powers up:

![check_1](gfx/charge_mod/05_check_switch.jpg)

+ Checking that the system will charge when switched off:

![check_2](gfx/charge_mod/06_charge.jpg)

**NOTE**:

It's possible, but trickier, to do this mod after having mounted your components, just cut the top trace near the switch and the trace between the BMS and the DCDC modules