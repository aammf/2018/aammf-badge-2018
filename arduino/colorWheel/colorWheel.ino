//Colour wheel demo sketch for the Aarhus Mini Maker Faire 2018 badge.
//Filters raw values from the MPU6050 and uses rotation around Z axis to make a colour wheel.
//Inherited licenses apply, check the github links.

#ifdef __AVR__
  #include <avr/power.h>
#endif

//Neopixel stuff:
#include <Adafruit_NeoPixel.h> // install via library manager, or from https://github.com/adafruit/Adafruit_NeoPixel
#define PIN 7
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(10, PIN, NEO_GRB + NEO_KHZ800);

//MPU6050 stuff:
#include "I2Cdev.h"  // https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/I2Cdev
#include "MPU6050.h" // https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/MPU6050
MPU6050 accelgyro;
int16_t ax, ay, az, axFiltered, ayFiltered, azFiltered;
int16_t gx, gy, gz, gxFiltered, gyFiltered, gzFiltered;

// CALIBRATION:
//put the badge on a flat surface, and try to get the gyro and ax+ay values to hit 0 
//az can see the earth's gravity, calibrate this by making sure the negative and positive values are about the same magnitude
//Measure az while flat, then flip the badge over, and use the average value between the two measurements. 
//These are my offset values, they're probably not gonna match yours.
int16_t axOffset=-1220;
int16_t ayOffset=-546;
int16_t azOffset=2385;
int16_t gxOffset=-30;
int16_t gyOffset=325;
int16_t gzOffset=-49;

//Filter the accel/gyro measurements with a pretty aggressive low-pass:
#include <MovingAverage.h> // https://github.com/sofian/MovingAverage
MovingAverage axFilter(0.01f);
MovingAverage ayFilter(0.01f);
MovingAverage azFilter(0.01f);
MovingAverage gxFilter(0.01f);
MovingAverage gyFilter(0.01f);
MovingAverage gzFilter(0.01f);


void setup() {
    Serial.begin(115200);

    //Fire up the MPU6050:
    Wire.begin();
    accelgyro.initialize();
    Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

    //Fire up the LEDs:
    pixels.begin();
    pixels.show(); // Initialize all pixels to 'off'
}

uint16_t gzSum=0; //cummulation of raw gyro values around Z axis.
int16_t brightness=255;

void loop() {

updateAndFilterMPUAll(); //refresh the values from the MPU6050 and feed the filters.
        
if(millis()%25==0){ //every 25ms.... this is just a random choice.. play around with it.


        //shift between brightness mode and colour mode by holding the badge vertical or horizontal.
        //then change the values by rotating around the gear (z) axis 


        //brightness mode:
        if(abs(axFiltered)>abs(azFiltered) || abs(ayFiltered)>abs(azFiltered)) { //if not flat
          if(abs(gzFiltered)>10) brightness+=gzFiltered>>11; //take the angular acceleration and shift it down so it makes sense.
          if(brightness<1) brightness=1;
          if(brightness>254) brightness=255;
          pixels.setBrightness((uint8_t)brightness);

          //Serial.println(brightness);
        }
        //colour mode:
        else{       
          //compensate for gyro drift (by ignoring values under 10)
          //also divide change by 10, so the wheel moves slower.
          if(abs(gzFiltered)>10) gzSum+=gzFiltered/10;      
          uint32_t colour = Wheel(gzSum>>8); //from uint16_t     to   uint8_t  and then to uint32_t... sorry.
                                            //   gyro_filtered  -> input to wheel() -> GRB colour code
          for(int i=0; i< pixels.numPixels(); i++) pixels.setPixelColor(i, colour);
        
/*
        Serial.print("Rotation around Z axis: ");
        Serial.println(gzSum);
        Serial.println("Resulting colour values: ");
        Serial.print("Red: "); Serial.println(colour>>8&0b11111111);
        Serial.print("Green: "); Serial.println(colour>>16&0b11111111);
        Serial.print("Blue: "); Serial.println(colour&0b11111111);
        Serial.println();
*/      
        }

    pixels.show();

    }


}


void updateAndFilterMPUAll()
{
        accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

        //Add calibration offsets:
        ax+=axOffset;
        ay+=ayOffset;
        az+=azOffset;
        gx+=gxOffset;
        gy+=gyOffset;
        gz+=gzOffset;

        //Feed the filters and refresh filtered values:
        axFiltered = axFilter.update(ax);
        ayFiltered = ayFilter.update(ay);
        azFiltered = azFilter.update(az);
        gxFiltered = gxFilter.update(gx);
        gyFiltered = gyFilter.update(gy);
        gzFiltered = gzFilter.update(gz);

        //Debug printout for calibration:
        //Serial.print(axFiltered); Serial.print("\t");
        //Serial.print(ayFiltered); Serial.print("\t");
        //Serial.print(azFiltered); Serial.print("\t");
        //Serial.print(gxFiltered); Serial.print("\t");
        //Serial.print(gyFiltered); Serial.print("\t");
        //Serial.print(gzFiltered);
        //Serial.println();
}

void updateAndFilterMPUGyroX()
{
  gxFiltered = gxFilter.update(accelgyro.getRotationX()+gxOffset);
}
void updateAndFilterMPUGyroY()
{
  gyFiltered = gyFilter.update(accelgyro.getRotationY()+gyOffset);
}
void updateAndFilterMPUGyroZ()
{
  gzFiltered = gzFilter.update(accelgyro.getRotationZ()+gzOffset);
}
void updateAndFilterMPUAccX()
{
  axFiltered = axFilter.update(accelgyro.getAccelerationX()+axOffset);
}
void updateAndFilterMPUAccY()
{
  ayFiltered = ayFilter.update(accelgyro.getAccelerationY()+ayOffset);
}
void updateAndFilterMPUAccZ()
{
  azFiltered = azFilter.update(accelgyro.getAccelerationZ()+azOffset);
}




// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
