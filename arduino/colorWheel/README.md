# ColorWheel
Colour wheel demo sketch for the Aarhus Mini Maker Faire 2018 badge.

Filters raw values from the MPU6050 and uses rotation around Z axis to make a colour wheel.
Inherited licenses apply, check the github links.

# Calibration
Put the badge on a flat surface, and try to get the gyro and ax+ay values to hit 0 

az can see the earth's gravity, calibrate this by making sure the negative and positive values are about the same magnitude

Measure az while flat, then flip the badge over, and use the average value between the two measurements. 

# Operation
The gear has two modes of operation determined by it's orientation:

Horizontal (flat) is colour mode.
Vertical (hanging) is brightness mode.

Change the colour/brigthness by rotating the badge around the gear axis (The Z axis)