#pragma once

#include "avr8gpio.h"

/*
| Define         | AVR | Pin | Description |
|----------------|-----|-----|-------------|
|                | PD3 | 1   | RX |
|                | PD2 | 2   | TX |
|                | PD1 | 5   | SDA         |
|                | PD0 | 6   | SCL         |
|                | PD4 | 7   |             |
|                | PC6 | 8   |             |
|                | PD7 | 9   |             |
|                | PE6 | 10  | WS2812B LEDS |
|                | PB4 | 11  |             |
|                | PB5 | 12  |             |
|                | PB6 | 13  |             |
|                | PB2 | 14  |             |
|                | PB3 | 15  |             |
|                | PB1 | 16  |             |
|                | PF7 | 17  |             |
*/

#define TWI_HARDWARE
#define TWI_SDA         GPD1
#define TWI_SCL         GPD0

#define LED_YELLOW     GPB0
#define LED_GREEN      GPD5

#define LED_STRING     GPE6
