#ifndef I2C
#define I2C

#include <avr/io.h>
#include <inttypes.h>

void i2c_init(void);

uint8_t i2c_start(uint8_t address);
uint8_t i2c_write(uint8_t data);
unsigned char i2c_readAck(void);
unsigned char i2c_readNak(void);

void i2c_stop(void);



#endif
