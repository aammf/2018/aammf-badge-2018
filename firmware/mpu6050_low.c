/* Name: mpu6050_low.c
 * Author: Zhyhariev Mikhail
 * License: MIT
 */
#include <inttypes.h>
#include <stdlib.h>
#include "mpu6050.h"
#include "i2c.h"


// Registers address
#define XG_OFFS_TC          0x00 //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
#define YG_OFFS_TC          0x01 //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
#define ZG_OFFS_TC          0x02 //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
#define X_FINE_GAIN         0x03 //[7:0] X_FINE_GAIN
#define Y_FINE_GAIN         0x04 //[7:0] Y_FINE_GAIN
#define Z_FINE_GAIN         0x05 //[7:0] Z_FINE_GAIN
#define XA_OFFS_H           0x06 //[15:0] XA_OFFS
#define XA_OFFS_L_TC        0x07
#define YA_OFFS_H           0x08 //[15:0] YA_OFFS
#define YA_OFFS_L_TC        0x09
#define ZA_OFFS_H           0x0A //[15:0] ZA_OFFS
#define ZA_OFFS_L_TC        0x0B
#define XG_OFFS_USRH        0x13 //[15:0] XG_OFFS_USR
#define XG_OFFS_USRL        0x14
#define YG_OFFS_USRH        0x15 //[15:0] YG_OFFS_USR
#define YG_OFFS_USRL        0x16
#define ZG_OFFS_USRH        0x17 //[15:0] ZG_OFFS_USR
#define ZG_OFFS_USRL        0x18
#define SMPLRT_DIV          0x19
#define CONFIG              0x1A
#define GYRO_CONFIG         0x1B
#define ACCEL_CONFIG        0x1C
#define FF_THR              0x1D
#define FF_DUR              0x1E
#define MOT_THR             0x1F
#define MOT_DUR             0x20
#define ZRMOT_THR           0x21
#define ZRMOT_DUR           0x22
#define FIFO_EN             0x23
#define I2C_MST_CTRL        0x24
#define I2C_SLV0_ADDR       0x25
#define I2C_SLV0_REG        0x26
#define I2C_SLV0_CTRL       0x27
#define I2C_SLV1_ADDR       0x28
#define I2C_SLV1_REG        0x29
#define I2C_SLV1_CTRL       0x2A
#define I2C_SLV2_ADDR       0x2B
#define I2C_SLV2_REG        0x2C
#define I2C_SLV2_CTRL       0x2D
#define I2C_SLV3_ADDR       0x2E
#define I2C_SLV3_REG        0x2F
#define I2C_SLV3_CTRL       0x30
#define I2C_SLV4_ADDR       0x31
#define I2C_SLV4_REG        0x32
#define I2C_SLV4_DO         0x33
#define I2C_SLV4_CTRL       0x34
#define I2C_SLV4_DI         0x35
#define I2C_MST_STATUS      0x36
#define INT_PIN_CFG         0x37
#define INT_ENABLE          0x38
#define DMP_INT_STATUS      0x39
#define INT_STATUS          0x3A
#define ACCEL_XOUT_H        0x3B
#define ACCEL_XOUT_L        0x3C
#define ACCEL_YOUT_H        0x3D
#define ACCEL_YOUT_L        0x3E
#define ACCEL_ZOUT_H        0x3F
#define ACCEL_ZOUT_L        0x40
#define TEMP_OUT_H          0x41
#define TEMP_OUT_L          0x42
#define GYRO_XOUT_H         0x43
#define GYRO_XOUT_L         0x44
#define GYRO_YOUT_H         0x45
#define GYRO_YOUT_L         0x46
#define GYRO_ZOUT_H         0x47
#define GYRO_ZOUT_L         0x48
#define EXT_SENS_DATA_00    0x49
#define EXT_SENS_DATA_01    0x4A
#define EXT_SENS_DATA_02    0x4B
#define EXT_SENS_DATA_03    0x4C
#define EXT_SENS_DATA_04    0x4D
#define EXT_SENS_DATA_05    0x4E
#define EXT_SENS_DATA_06    0x4F
#define EXT_SENS_DATA_07    0x50
#define EXT_SENS_DATA_08    0x51
#define EXT_SENS_DATA_09    0x52
#define EXT_SENS_DATA_10    0x53
#define EXT_SENS_DATA_11    0x54
#define EXT_SENS_DATA_12    0x55
#define EXT_SENS_DATA_13    0x56
#define EXT_SENS_DATA_14    0x57
#define EXT_SENS_DATA_15    0x58
#define EXT_SENS_DATA_16    0x59
#define EXT_SENS_DATA_17    0x5A
#define EXT_SENS_DATA_18    0x5B
#define EXT_SENS_DATA_19    0x5C
#define EXT_SENS_DATA_20    0x5D
#define EXT_SENS_DATA_21    0x5E
#define EXT_SENS_DATA_22    0x5F
#define EXT_SENS_DATA_23    0x60
#define MOT_DETECT_STATU    0x61
#define I2C_SLV0_DO         0x63
#define I2C_SLV1_DO         0x64
#define I2C_SLV2_DO         0x65
#define I2C_SLV3_DO         0x66
#define I2C_MST_DELAY_CTRL  0x67
#define SIGNAL_PATH_RESET   0x68
#define MOT_DETECT_CTRL     0x69
#define USER_CTRL           0x6A
#define PWR_MGMT_1          0x6B
#define PWR_MGMT_2          0x6C
#define BANK_SEL            0x6D
#define MEM_START_ADDR      0x6E
#define MEM_R_W             0x6F
#define DMP_CFG_1           0x70
#define DMP_CFG_2           0x71
#define FIFO_COUNTH         0x72
#define FIFO_COUNTL         0x73
#define FIFO_R_W            0x74
#define WHO_AM_I            0x75

// Pin AD0 connected to ground
#define MPU6050_W           0xD0
#define MPU6050_R           MPU6050_W+1

int8_t mpu6050_read_8(uint8_t reg);
int16_t mpu6050_read_16(uint8_t reg);
void mpu6050_start_read(uint8_t reg);
void mpu6050_write(uint8_t reg, uint8_t value);


void MPU6050_Init(void) {
  // I2C Initialize
  i2c_init();

  //Sets sample rate to 8000/1+7 = 1000Hz
  mpu6050_write(SMPLRT_DIV, 0x07);

  //Disable FSync, 256Hz DLPF
  mpu6050_write(CONFIG, 0x00);

  //Disable gyro self tests, scale of 2000 degrees/s
  mpu6050_write(GYRO_CONFIG, 0x18);

  //Disable accel self tests, scale of +-2g, no DHPF
  mpu6050_write(ACCEL_CONFIG, 0x00);

  //Freefall threshold of |0mg|
  mpu6050_write(FF_THR, 0x00);

  //Freefall duration limit of 0
  mpu6050_write(FF_DUR, 0x00);

  //Motion threshold of 0mg
  mpu6050_write(MOT_THR, 0x00);

  //Motion duration of 0s
  mpu6050_write(MOT_DUR, 0x00);

  //Zero motion threshold
  mpu6050_write(ZRMOT_THR, 0x00);

  //Zero motion duration threshold
  mpu6050_write(ZRMOT_DUR, 0x00);

  //Disable sensor output to FIFO buffer
  mpu6050_write(FIFO_EN, 0x00);

  //AUX I2C setup
  //Sets AUX I2C to single master control, plus other config
  mpu6050_write(I2C_MST_CTRL, 0x00);
  //Setup AUX I2C slaves
  mpu6050_write(I2C_SLV0_ADDR, 0x00);
  mpu6050_write(I2C_SLV0_REG, 0x00);
  mpu6050_write(I2C_SLV0_CTRL, 0x00);
  mpu6050_write(I2C_SLV1_ADDR, 0x00);
  mpu6050_write(I2C_SLV1_REG, 0x00);
  mpu6050_write(I2C_SLV1_CTRL, 0x00);
  mpu6050_write(I2C_SLV2_ADDR, 0x00);
  mpu6050_write(I2C_SLV2_REG, 0x00);
  mpu6050_write(I2C_SLV2_CTRL, 0x00);
  mpu6050_write(I2C_SLV3_ADDR, 0x00);
  mpu6050_write(I2C_SLV3_REG, 0x00);
  mpu6050_write(I2C_SLV3_CTRL, 0x00);
  mpu6050_write(I2C_SLV4_ADDR, 0x00);
  mpu6050_write(I2C_SLV4_REG, 0x00);
  mpu6050_write(I2C_SLV4_DO, 0x00);
  mpu6050_write(I2C_SLV4_CTRL, 0x00);
  mpu6050_write(I2C_SLV4_DI, 0x00);
  //I2C_MST_STATUS //Read-only
  //Setup INT pin and AUX I2C pass through
  mpu6050_write(INT_PIN_CFG, 0x00);
  //Enable data ready interrupt
  mpu6050_write(INT_ENABLE, 0x00);

  //Slave out, dont care
  mpu6050_write(I2C_SLV0_DO, 0x00);
  mpu6050_write(I2C_SLV1_DO, 0x00);
  mpu6050_write(I2C_SLV2_DO, 0x00);
  mpu6050_write(I2C_SLV3_DO, 0x00);
  //More slave config
  mpu6050_write(I2C_MST_DELAY_CTRL, 0x00);
  //Reset sensor signal paths
  mpu6050_write(SIGNAL_PATH_RESET, 0x00);
  //Motion detection control
  mpu6050_write(MOT_DETECT_CTRL, 0x00);
  //Disables FIFO, AUX I2C, FIFO and I2C reset bits to 0
  mpu6050_write(USER_CTRL, 0x00);
  //Sets clock source to gyro reference w/ PLL
  mpu6050_write(PWR_MGMT_1, 0x02);
  //Controls frequency of wakeups in accel low power mode plus the sensor standby modes
  mpu6050_write(PWR_MGMT_2, 0x00);
}


void mpu6050_start_read(uint8_t reg) {
  i2c_start(MPU6050_W);
  i2c_write(reg);
  i2c_stop();
}

int16_t mpu6050_read_16(uint8_t reg) {
  mpu6050_start_read(reg);

  i2c_start(MPU6050_R);

  int16_t result = 0;

  result += i2c_readAck();      
  result <<= 8;
  
  result += i2c_readNak();

  i2c_stop();
  
  return result;
}

int8_t mpu6050_read_8(uint8_t reg) {
  mpu6050_start_read(reg);

  i2c_start(MPU6050_R);

  int16_t result = 0;

  result += i2c_readNak();

  i2c_stop();
  
  return result;
}


void mpu6050_write(uint8_t reg, uint8_t value) {
  i2c_start(MPU6050_W);
  i2c_write(reg);
  i2c_write(value);
  i2c_stop();
}


/**
 * Returning a value of "whoAmI" register MPU6050
 * @return  "whoAmI" register value
 */
uint8_t mpu6050_whoAmI(void) {
    return mpu6050_read_8(WHO_AM_I);
}

/**
 * Reads the temperature in deciCelcius (10 dC = 1C)
 **/
int16_t mpu6050_readTempDC() {
  int16_t v = mpu6050_read_16(TEMP_OUT_H);
  return v / 34 + 365;  
}

void mpu6050_read_a16(uint8_t reg, int16_t *buffer, uint8_t len) {
  while (len--) {
    *(buffer++) = mpu6050_read_16(reg);    
    reg += 2;
  }    
}

void mpu6050_readAccel(int16_t *buffer) {
    // Getting value of accelerometer registers for X, Y and Z axises
  mpu6050_read_a16(ACCEL_XOUT_H, buffer, 3);
}


/**
 * Getting a value of gyroscope registers MPU6050
 * @return  the array of gyroscope registers values
 */
void mpu6050_readGyro(int16_t *buffer) {
    // Getting value of gyroscope registers for X, Y and Z axises
  mpu6050_read_a16(GYRO_XOUT_H, buffer, 3);
}
