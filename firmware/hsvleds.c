#include <avr/pgmspace.h>
#include <string.h>

#include "hsvleds.h"
#include "light_ws2812.h"

const uint8_t PROGMEM gamma8[] = {
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
        1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
        2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
        5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
       10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
       17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
       25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
       37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
       51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
       69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
       90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
      115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
      144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
      177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
      215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };

// All the LED data, ready to spewed out over the GPIO pin
struct {
#if RGB_LEDS > 0
  struct cRGB led[RGB_LEDS];
#endif
#if RGBW_LEDS > 0  
  struct cRGBW rgbwled[RGBW_LEDS];
#endif
} __attribute__((packed)) allLeds ;

void initPixels(void) {
  memset(&allLeds, 0, sizeof(allLeds));
}


#if RGB_LEDS > 0
// Sets the color of a pixel as hsv values, with gamma correction
void setRGBPixel(uint8_t index, uint8_t h, uint16_t s, uint16_t v) {

  uint8_t r,g,b;
    
  if (s == 0) {
    // color is grayscale 
    r = g = b = v;
       
  } else {
    
    // make hue 0-5
    uint8_t region = h / 43;
    // find remainder part, make it from 0-255 
    uint16_t fpart = (h - (region * 43)) * 6;
    
    // calculate temp vars, doing integer multiplication 
    uint8_t p = (v * (255 - s)) >> 8;
    uint8_t q = (v * (255 - ((s * fpart) >> 8))) >> 8;
    uint8_t t = (v * (255 - ((s * (255 - fpart)) >> 8))) >> 8;
    
    // assign temp vars based on color cone region 
    switch(region) {
    case 0:
      r = v; g = t; b = p; break;
    case 1:
      r = q; g = v; b = p; break;
    case 2:
      r = p; g = v; b = t; break;
    case 3:
      r = p; g = q; b = v; break;
    case 4:
      r = t; g = p; b = v; break;
    default:
      r = v; g = p; b = q; break;
    }
  }
  
  allLeds.led[index].r = pgm_read_byte(&gamma8[r]);
  allLeds.led[index].g = pgm_read_byte(&gamma8[g]);
  allLeds.led[index].b = pgm_read_byte(&gamma8[b]);
}

#endif

#if RGBW_LEDS > 0
void setRGBWPixel(uint8_t index, uint8_t h, uint16_t s, uint16_t v) {

  uint8_t r,g,b;
    
  if (s == 0) {
    /* color is grayscale */
    r = g = b = v;
       
  } else {
    
    /* make hue 0-5 */
    uint8_t region = h / 43;
    /* find remainder part, make it from 0-255 */
    uint16_t fpart = (h - (region * 43)) * 6;
    
    /* calculate temp vars, doing integer multiplication */
    uint8_t p = (v * (255 - s)) >> 8;
    uint8_t q = (v * (255 - ((s * fpart) >> 8))) >> 8;
    uint8_t t = (v * (255 - ((s * (255 - fpart)) >> 8))) >> 8;
    
    /* assign temp vars based on color cone region */
    switch(region) {
    case 0:
      r = v; g = t; b = p; break;
    case 1:
      r = q; g = v; b = p; break;
    case 2:
      r = p; g = v; b = t; break;
    case 3:
      r = p; g = q; b = v; break;
    case 4:
      r = t; g = p; b = v; break;
    default:
      r = v; g = p; b = q; break;
    }
  }

  // Take out the monochrome part of the pixels and light up the white LED.
  uint8_t w = r;
  if (w > g)  {
    w = g;
  }
  if (w > b) {
    w = b;
  }
  r -= w;
  g -= w;
  b -= w;

  allLeds.rgbwled[index].w = pgm_read_byte(&gamma8[w]);  
  allLeds.rgbwled[index].r = pgm_read_byte(&gamma8[r]);
  allLeds.rgbwled[index].g = pgm_read_byte(&gamma8[g]);
  allLeds.rgbwled[index].b = pgm_read_byte(&gamma8[b]);
}

#endif

void sendPixels(void) {
    ws2812_sendarray((uint8_t *)&allLeds, sizeof(allLeds));
}
