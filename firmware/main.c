#include <avr/wdt.h> 
#include <stdio.h>
#include <util/delay.h>

#include "board.h"

#include "leds.h"
#include "uart.h"
#include "hsvleds.h"

#include "mpu6050.h"
#include <math.h>

int16_t avgA = 0;
int16_t avgB = 0;
int16_t avgC = 0;
int16_t accelRaw[3];

void readAccelerometer(void) {

  mpu6050_readAccel(accelRaw);  

  int16_t rawA = round(atan2(-accelRaw[2], accelRaw[1])*(1<<15)/M_PI_2);
  avgA += (rawA-avgA) >> 2;

  int16_t rawB = round(atan2(accelRaw[0], accelRaw[1])*(1<<15)/M_PI_2);
  avgB += (rawB-avgB) >> 2;

  int16_t rawC = round(atan2(accelRaw[0], accelRaw[2])*(1<<15)/M_PI_2);
  avgC += (rawC-avgC) >> 2;
}

#define MAX(a,b) (((a)>(b))?(a):(b))
#define ABS(a) ((a) < 0 ? -(a) : (a))

int main(void) {
  wdt_enable(WDTO_15MS);
  ledInit();
  ledBooting();
  uartInit();
  MPU6050_Init();
  L("Booting\r\n");
  initPixels();

  ledBooted();
  
  uint16_t loop = 0;
  uint8_t led = 0;
  uint16_t hueBase = 0;
  uint8_t brightLed = 0;      
  while (1) {
    wdt_reset();
    
    readAccelerometer();
    int16_t maxAccel = MAX(ABS(accelRaw[0]), ABS(accelRaw[1]));
    maxAccel = MAX(maxAccel, ABS(accelRaw[2]));
    
    if (loop++ > 30) {
      loop = 0;
      ledSet(led++);
      P("b:%d\tc:%d\tma:%d\n", avgB >> 8, avgA>>8, maxAccel);
    }

    if (maxAccel > 25000) {
      brightLed++;
      if (brightLed >= 10) {
	brightLed = 0;
      }
    }

    uint16_t flashy = 0;
    if (maxAccel > 20000) {
	flashy = ((maxAccel-20000) >> 5);
    }    

    uint16_t pixelHue = (avgA>>2)/RGBW_LEDS;
    hueBase += avgB >> 10;    
    uint16_t hue = hueBase;
    for (int i=0;i<RGBW_LEDS;i++) {
      uint16_t h = (hue >> 5) & 0xff;
      uint16_t s = 150;
      uint16_t v = 60;

      if (i == brightLed && flashy) {
	v += flashy;
      }
      
      setRGBWPixel(i, h, s, v);

      hue += pixelHue;
    }
    
    sendPixels();
  }
}


