#pragma once
#include <inttypes.h>

// The number of RGB LEDs we have at the start of the string
#define RGB_LEDS 0

// The number of RGBW LEDs we have after the RGB leds
#define RGBW_LEDS 10

void initPixels(void);

#if RGB_LEDS > 0
void setRGBPixel(uint8_t index, uint8_t h, uint16_t s, uint16_t v);
#endif

#if RGBW_LEDS > 0
void setRGBWPixel(uint8_t index, uint8_t h, uint16_t s, uint16_t v);
#endif

void sendPixels(void);
