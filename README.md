# aammf-badge-2018

The Badge for Aarhus Mini Maker Faire 2018

![Front view](out/render-2018-09-14-front.png)
![Back view](out/render-2018-09-14-back.png)

If you don't feel like opening the kicad project, then
[the schematic is included as pdf too](out/schematic-2018-09-14.pdf)

The gerbers used for the final version are in:
[aammf2018-2018-09-14.zip](out/aammf2018-2018-09-14.zip)

# Revisions

* 2018-08-24: First PCB prototype
* 2018-08-28: Added SMD switch
* 2018-08-29: Added Shitty addon for hats.
* 2018-09-05: Fixed gnd to the shitty addon header trace
* 2018-09-06: Fixed +in/gnd swap for the boost converter
* 2018-09-12
  * ReFixed +in/gnd swap for the boost converter
  * Larger holes for Arduino
  * Larger holes for gyro
  * Rotated Arduino to allow access to the USB port
* 2018-09-14
  * Re-wired the lipo-charger to pick up power from the pro-micro USB.
  * Added PCBWAY sposor logos.
  * Made the Lipo charger and the boost converter though-hole componets.
  * Added the first hello-world firmware skeleton.


# ebay-BOM

These are the parts used on the badge:

* pro micro | https://www.ebay.com/sch/i.html?_nkw=atmega32u4+pro+micro
* boost converter | https://www.ebay.com/itm/322677626844
* lipo charger | https://www.ebay.com/itm/182937957257 https://www.ebay.com/itm/132751361583
* gy-521 | https://www.ebay.com/itm/153030528330
* Addressable LEDs in 5050 housing, either:
  * sk6812 | https://www.ebay.com/itm/291781682522
  * ws2812b | https://www.ebay.com/itm/252319870385
* Switch | https://www.ebay.com/itm/232687558560


# QR code

* Generate QR code in PNG using: https://zxing.appspot.com/generator/
* Select Barcode size Large.
* Use bitmap2component to generate footprint from png.
* Adjust DPI to 700 or so to give the desired output size

# svg to kicad module

cd kicad

svg2mod -i ../gfx/cog-badge-outline-spacy.svg --format pretty --name cogbadge-outline-spacy2 -o cogbadge-outline-spacy2 --p 1

# Construction

The parts, except for the pro-micros, are available at cost from the OSAA booth.


## Mount LEDS

First mount the LEDs on the front of the badge along with the de-coupling caps, it's hard to do that with stuff on the back.

![Front view](gfx/complete-front.jpg)


## Mount the boost converter

The first module to mount on the back should be the boost converter and it needs to be mounted flush to the board, so the acclerometer can overlap it.

## Mount the accelerometer after the boost converter

Don't mount the acclerometer before the boost converter.

![Boost Vs. Accel](gfx/boost-converter-under-accelerometer.jpg)

## Mount the 4056BMS charger/battery management module

Unless you make the charge-while-off mod described later, mount the charger flush to the board too, you're not going to need the micro usb connector on this board at all.

![Charger](gfx/charger.jpg)


## Mount everything else

![Back view](gfx/complete-back.jpg)


The rest of the parts on the back can be mounted in any order.

## Charge though the pro-micro

To be able to charge the battery via the pro-micro the J1 solder jumper on the pro-micro should be connected to the J1 though-hole pad next to it.

If you don't connect the J1 connection, then you'll need two micro-USB cables to charge and power the badge at the same time.

You can do this without interfering with the charge-while-off mod.

![j1-wire](gfx/j1-wire.jpg)

When the j1 wire has been added the power circuit will look like this:

![j1-hack](gfx/j1-hack.png)



# Modifying the board so it will charge while off [MEDIUM/HARD difficulty]

The board has been designed so that the power switch toggles the connection to the battery, leaving the board unable to charge through the 4056BMS when the switch is set to 'OFF'.

A slight modification to the board is required to enable charging while off. Difficulty: Medium to hard.

[The guide is here](charge_mod.md)

After this mod, it will still be possible to charge though the connector on the pro micro, with the badge on, in addition to being able to charge the battery without the badge blinking away via the otherwise unused connector on the charger.

